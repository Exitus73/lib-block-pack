# LibBlockPack #
Simple packing algorithm for packing square shapes into larger square.
Built for making Sprite Sheets.

## INSTALL ##


```
#!php

composer require exitus/lib-block-pack
```


## USAGE: ##

### 1. Build list of blocks ###


```
#!php

$blockList=new Exitus\LibBlockPack\BlockList();
$blockList->add(
        new Exitus\LibBlockPack\Block($id/filename,$width,$height)
    );
$blockList->add(
        new Exitus\LibBlockPack\Block($id/filename,$width,$height)
    );
```

### 2. run packer with blockList ###

```
#!php

$packer=new Exitus\LibBlockPack\Packer($blockList);
$packer->pack();
```


### 3. get X and Y positions for  blocks ###

```
#!php

foreach ($blockList->getBlocks() as $block) {
	$block->getId();
	$block->getX();
	$block->getY();

	// also methods available if needed
	$block->getWidth();
	$block->getHeight();
}
```