<?php
/**
 * Created by PhpStorm.
 * User: Jukka
 * Date: 26.1.2016
 * Time: 18:39
 */

namespace Exitus\LibBlockPack;

class Block
{
    private $id;
    private $width;
    private $height;

    private $x=null;
    private $y=null;

    public function __construct($id,$width,$height) {
        $this->id=$id;
        $this->width=$width;
        $this->height=$height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return mixed
     */
    public function getRadius()
    {
        return $this->height*2+$this->width*2;
    }

    /**
     * @return null
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param null $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * @return null
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param null $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}