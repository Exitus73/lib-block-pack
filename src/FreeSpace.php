<?php
/**
 * Created by PhpStorm.
 * User: Jukka
 * Date: 31.1.2016
 * Time: 5:54
 */

namespace Exitus\LibBlockPack;


class FreeSpace
{
    private $totalHeight;
    private $totalWidth;

    private $space=[];

    public function __construct($height)
    {
        $this->totalHeight=$height;
    }


    /**
     * @param $x
     * @param $width
     * @param $height
     */
    public function setUsedSpace($x, $width, $height) {
        if (isset($this->space[$x])) {
            //check if width is lower, make new index
            if ($this->space[$x]['width']>$width) {
                $this->space[$x+$width]=[
                    'availableHeight'=>$this->space[$x]['availableHeight'],
                ];
            }
            $this->space[$x]['availableHeight'] -= $height;
        } else {
            $this->space[$x]=[
                'availableHeight'=>$this->getTotalHeight() - $height,
            ];
        }

        $this->cleanUpSpace();
    }


    private function cleanUpSpace() {
        $this->removeDuplicates();
        $this->recalculateWidths();
    }

    private function removeDuplicates() {
        ksort($this->space);
        //remove empty from beginning
        foreach ($this->space as $key=>$space) {
            if ($space['availableHeight'] == 0) {
                unset($this->space[$key]);
                continue;
            } else {
                break;
            }
        }

        $previousX=null;
        foreach ($this->space as $key=>$space) {

            if (isset($previousX)) {
                if ($space['availableHeight']==$this->space[$previousX]['availableHeight']) {
                    unset($this->space[$key]);
                    continue;
                }
            }
            $previousX=$key;
        }
    }


    private function recalculateWidths() {
        ksort($this->space);
        $previousX=null;
        foreach ($this->space as $key=>$space) {
            if (isset($previousX)) {
                $this->space[ $previousX ]['width']=$key-$previousX;
            }
            $previousX=$key;
        }
        //set last
        if (isset($previousX)) {
            $this->space[ $previousX ]['width']=$this->getTotalWidth()-$previousX;
        }
    }

    /**
     * @param $width
     * @param $height
     * @return array|null
     */
    public function findSpace($width, $height) {
        ksort($this->space);
        foreach ($this->space as $key=>$space) {
            if ($space['availableHeight'] >= $height && $space['width']>=$width) {
                return [
                    'x'=>$key,
                    'y'=>$this->totalHeight-$space['availableHeight'],
                ];
            }
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getTotalHeight()
    {
        return $this->totalHeight;
    }

    /**
     * @return mixed
     */
    public function getTotalWidth()
    {
        return $this->totalWidth;
    }

    /**
     * @param mixed $totalWidth
     */
    public function setTotalWidth($totalWidth)
    {
        $this->totalWidth = $totalWidth;
    }
}