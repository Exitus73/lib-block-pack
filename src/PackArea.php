<?php
/**
 * Created by PhpStorm.
 * User: Jukka
 * Date: 27.1.2016
 * Time: 18:45
 */

namespace Exitus\LibBlockPack;


class PackArea
{
    private $width=0;
    private $height=0;

    private $freeSpace;

    public function __construct($height) {
        $this->setHeight($height);
        $this->freeSpace=new FreeSpace($height);
    }

    /**
     * @param $width
     */
    public function growWidthBy($width) {
        $this->setWidth( $this->getWidth()+$width );
        $this->freeSpace->setTotalWidth( $this->getWidth());
    }


    /**
     * @param Block $block
     */
    public function placeBlock(Block $block) {
        $x=null;
        $y=null;
        $position=$this->getFreeSpace()->findSpace($block->getWidth(),$block->getHeight());
        if (isset($position)) {
            $x=$position['x'];
            $y=$position['y'];

        } else {
            $x=$this->getWidth();
            $y=0;
            $this->growWidthBy($block->getWidth());
        }
        $block->setY($y);
        $block->setX($x);
        $this->getFreeSpace()->setUsedSpace(
            $block->getX(),
            $block->getWidth(),
            $block->getHeight()
        );
    }


    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }



    /**
     * @return FreeSpace
     */
    public function getFreeSpace()
    {
        return $this->freeSpace;
    }


}