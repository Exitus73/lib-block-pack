<?php
/**
 * Created by PhpStorm.
 * User: Jukka
 * Date: 26.1.2016
 * Time: 18:07
 */

namespace Exitus\LibBlockPack;

class Packer
{
    private $blockQueue;
    private $packArea;


    public function __construct(BlockList $blockQueue) {
        $this->blockQueue= $blockQueue;
        $this->setPackArea(  new PackArea($blockQueue->getMaxHeight()) );

    }

    /**
     * @param Block $block
     */
    public function addBlock(Block $block) {
        $this->blockQueue->add($block);
    }

    /**
     * @return BlockList
     */
    public function getBlockQueue()
    {
        return $this->blockQueue;
    }

    /**
     *
     */
    public function pack() {
        $this->getBlockQueue()->initForPack();
        while ($block=$this->getBlockQueue()->getNext()) {
           $this->getPackArea()->placeBlock($block);
        }

    }

    /**
     * @return PackArea
     */
    public function getPackArea()
    {
        return $this->packArea;
    }

    /**
     * @param PackArea $packArea
     */
    public function setPackArea($packArea)
    {
        $this->packArea = $packArea;
    }

}