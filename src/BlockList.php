<?php
/**
 * Created by PhpStorm.
 * User: Jukka
 * Date: 26.1.2016
 * Time: 18:53
 */

namespace Exitus\LibBlockPack;

class BlockList
{
    private $queue=[];
    private $maxHeight=0;
    private $currentIndex=0;

    public function initForPack() {
        $this->sort();
        $this->currentIndex=0;
    }

    /**
     * @return Block[]
     */
    public function getBlocks() {
        return $this->queue;
    }

    public function add(Block $block) {
        $this->queue[]=$block;
        if ( $block->getHeight() > $this->getMaxHeight() ) {
            $this->setMaxHeight( $block->getHeight() );
        }
    }

    /**
     * @return Block|null
     */
    public function getNext() {
        if ($this->hasMore()) {
            return $this->queue[$this->currentIndex++];
        }
        return null;
    }

    private function hasMore() {
        if (count($this->queue) > $this->currentIndex) {
            return true;
        }
        return false;
    }


    private function sort() {
        usort($this->queue,array($this,'sorter'));
    }

    /**
     * @return int
     */
    public function getMaxHeight()
    {
        return $this->maxHeight;
    }

    /**
     * @param int $maxHeight
     */
    public function setMaxHeight($maxHeight)
    {
        $this->maxHeight = $maxHeight;
    }

    private function sorter(Block $a,Block $b) {
        if ($a->getHeight()==$b->getHeight()) {
            if ($a->getWidth()==$b->getWidth()) {
                return 0;
            }
            if ($a->getWidth()>$b->getWidth()) {
                return -1;
            } else {
                return 1;
            }
        }
        if ($a->getHeight()>$b->getHeight()) {
            return -1;
        } else {
            return 1;
        }
    }

}